// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleShooter.h"
#include "Weapon.h"


// Sets default values
AWeapon::AWeapon() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Always replicate this actor
	bReplicates = true;

	// Set up the model
	Model = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	Model->SetOnlyOwnerSee(true);            // only the owning player will see this mesh
	Model->bCastDynamicShadow = false;
	Model->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	Model->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay() {
	Super::BeginPlay();

	// Start with a full magazine
	Reload();
}

// Called every frame
void AWeapon::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void AWeapon::FireWeapon() {
	// TODO some placeholder or something
}

// Send our replicated properties
void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	// TODO only to sever
	DOREPLIFETIME(AWeapon, Ammo);
}

int AWeapon::GetMaxAmmo() {
	return MaxAmmo;
}

bool AWeapon::CanFire() {
	return Ammo > 0;
}

bool AWeapon::Reload_Validate() {
	return true; // TODO validate
}

void AWeapon::Reload_Implementation() {
	Ammo = MaxAmmo;
}

void AWeapon::OnFire_Implementation() {
	Ammo--;
	BroadcastFireEffects();
}

void AWeapon::BroadcastFireEffects_Implementation() {
	FireEffects();
}

void AWeapon::FireEffects_Implementation() {
}

