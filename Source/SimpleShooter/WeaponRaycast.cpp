// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleShooter.h"
#include "WeaponRaycast.h"
#include "SimpleShooterCharacter.h"

void AWeaponRaycast::FireWeapon() {
	// Make sure we have a world
	UWorld *const World = GetWorld();
	if (World == NULL) return;

	// Make sure we have a parent
	AActor *parent = GetAttachParentActor();
	if (parent == NULL) return;

	// TODO the maximum weapon range
	float PlayerInteractionDistance = 500;

	// get the camera transform
	FVector CameraLoc;
	FRotator CameraRot;
	parent->GetActorEyesViewPoint(CameraLoc, CameraRot);

	// Find the start and end points of the ray
	FVector Start = CameraLoc;
	FVector End = CameraLoc + (CameraRot.Vector() * PlayerInteractionDistance);

	// Configure the raycast
	FCollisionQueryParams RV_CastParams = FCollisionQueryParams(FName(TEXT("RV_ShootCast")), true, this);
	RV_CastParams.bTraceComplex = true;
	RV_CastParams.bTraceAsyncScene = true;
	RV_CastParams.bReturnPhysicalMaterial = true;

	// Don't collide with the parent
	RV_CastParams.AddIgnoredActor(parent);

	// Somewhere to put the results
	FHitResult RV_Hit(ForceInit);

	//  do the raycast
	bool DidCollide = World->LineTraceSingleByChannel(
			RV_Hit,        //result
			Start,        //start
			End,        //end
			ECC_Pawn,    //collision channel
			RV_CastParams
	);

	if (DidCollide) {
		AActor *hit = RV_Hit.GetActor();
		ASimpleShooterCharacter *other = Cast<ASimpleShooterCharacter>(hit);

		if (other != NULL) {
			// TODO move to ue4 damage system
			other->DealDamage(15);
		}
	}

	OnFire();
}
