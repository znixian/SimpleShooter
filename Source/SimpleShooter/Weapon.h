// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))

class SIMPLESHOOTER_API AWeapon : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon();

	/** The model of the weapon */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)

	class USkeletalMeshComponent *Model;

public:

	/** The weapon's current ammo */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Weapon")
	int Ammo;

	UFUNCTION(BlueprintPure)

	int GetMaxAmmo();

	/** Can this weapon be fired? */
	UFUNCTION(BlueprintPure, Category = "Weapon")

	bool CanFire();

	/** Reload this weapon */
	// TODO maybe use BlueprintNativeEvent
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Weapon")

	void Reload();

	/** Fire this weapon */
	virtual void FireWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** The maximum amount of ammo that can fit into a single magazine */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	int MaxAmmo;

	/** Run when the weapon is fired. For example, decrease the ammo and calls FireEffects */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Weapon")

	void OnFire();

	/**
	 * Fire effects on all connected clients
	 */
	// TODO does this need to be reliable or not?
	UFUNCTION(NetMulticast, Category = "Weapon", reliable)

	void BroadcastFireEffects();

	/**
	 * Run any effects that should be run when this weapon is fired.
	 * This is run on all the clients, so sounds, etc can be emitted.
	 * For example, play sounds or particle effects.
	 */
	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")

	void FireEffects();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
