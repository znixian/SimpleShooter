// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "Weapon.h"
#include "GameFramework/Character.h"
#include "SimpleShooterCharacter.generated.h"

class UInputComponent;

UCLASS(config = Game)

class ASimpleShooterCharacter : public ACharacter {
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)

	class USkeletalMeshComponent *Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))

	class UCameraComponent *FirstPersonCameraComponent;

public:
	ASimpleShooterCharacter();

protected:
	virtual void BeginPlay();

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Weapons")

	class AWeapon *Weapon;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)

	class UAnimMontage *FireAnimation;

	/** The player's current health */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Damage")
	int Health;

public:

	/** Get the player's current health, as a fraction of the maximum health */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Damage")

	float GetHealthProportion();

	/** Deal a certain amount of damage to a player */
	UFUNCTION(BlueprintCallable, Category = "Damage")

	// TODO move to UE4 damage system
	void DealDamage(int amount);

	/** Switch to a certain weapon */
	UFUNCTION(BlueprintCallable, Category = "Weapons")

	void SetWeapon(class AWeapon *NewWeapon);

	/** Get the player's current ammo, as a fraction of the maximum ammo */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapons")

	float GetAmmoProportion();

public:
	UFUNCTION(Server, Reliable, WithValidation)

	void FireWeapon();

protected:

	/** Fires a projectile. */
	void OnFire();

	/** Reload the weapon. */
	void Reload();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent *InputComponent) override;
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent *GetMesh1P() const { return Mesh1P; }

	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent *GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

