// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon.h"
#include "WeaponRaycast.generated.h"

/**
 * Abstract weapon that uses a raycast to find it's target
 */
UCLASS(meta = (BlueprintSpawnableComponent))

class SIMPLESHOOTER_API AWeaponRaycast : public AWeapon {
	GENERATED_BODY()

public:

	/** Fire this weapon */
	virtual void FireWeapon() override;

};
