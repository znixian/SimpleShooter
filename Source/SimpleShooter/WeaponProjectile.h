// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon.h"
#include "WeaponProjectile.generated.h"

/**
 * Abstract weapon which launches a projectile when fired
 */
UCLASS(meta = (BlueprintSpawnableComponent))

class SIMPLESHOOTER_API AWeaponProjectile : public AWeapon {
	GENERATED_BODY()

public:

	AWeaponProjectile();

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ASimpleShooterProjectile> ProjectileClass;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)

	class USceneComponent *MuzzleLocation;

	/** Fire this weapon */
	virtual void FireWeapon() override;

};
