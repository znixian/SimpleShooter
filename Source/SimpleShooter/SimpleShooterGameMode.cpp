// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SimpleShooter.h"
#include "SimpleShooterGameMode.h"
#include "SimpleShooterHUD.h"
#include "SimpleShooterCharacter.h"

ASimpleShooterGameMode::ASimpleShooterGameMode()
		: Super() {
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
			TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASimpleShooterHUD::StaticClass();
}
