// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleShooter.h"
#include "WeaponProjectile.h"
#include "SimpleShooterCharacter.h"
#include "SimpleShooterProjectile.h"

AWeaponProjectile::AWeaponProjectile() {
	// Make a muzzle location node thing
	MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	MuzzleLocation->SetupAttachment(Model);
}

void AWeaponProjectile::FireWeapon() {
	// Make sure we have a parent
	ASimpleShooterCharacter *parent = CastChecked<ASimpleShooterCharacter>(GetAttachParentActor());
	if (parent == NULL) return;

	// try and fire a projectile
	if (ProjectileClass != NULL) {
		UWorld *const World = GetWorld();
		if (World != NULL) {
			// Get the rotation of the player
			const FRotator SpawnRotation = parent->GetControlRotation();

			// Get the in-world position of the muzzle
			const FVector SpawnLocation = MuzzleLocation->GetComponentLocation();

			// Set the spawning parameters
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride =
					ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<ASimpleShooterProjectile>
					(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

			OnFire();
		}
	}
}

