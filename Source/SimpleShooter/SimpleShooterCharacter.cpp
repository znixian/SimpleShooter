// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SimpleShooter.h"
#include "SimpleShooterCharacter.h"
#include "SimpleShooterProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Engine.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ASimpleShooterCharacter

ASimpleShooterCharacter::ASimpleShooterCharacter() {
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
}

void ASimpleShooterCharacter::BeginPlay() {
	// Call the base class  
	Super::BeginPlay();

	Mesh1P->SetHiddenInGame(false, true);

	Health = 100;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASimpleShooterCharacter::SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) {
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASimpleShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASimpleShooterCharacter::MoveRight);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASimpleShooterCharacter::OnFire);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ASimpleShooterCharacter::Reload);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASimpleShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASimpleShooterCharacter::LookUpAtRate);
}

void ASimpleShooterCharacter::OnFire() {
	// Don't try to fire if we don't have a weapon
	if (Weapon == NULL) return;

	// Don't try to fire if we're out of ammo.
	if (Weapon->Ammo <= 0) return;

	// Raycast on the server
	FireWeapon();

	// try and play a firing animation if specified
	if (FireAnimation != NULL) {
		// Get the animation object for the arms mesh
		UAnimInstance *AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL) {
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void ASimpleShooterCharacter::Reload() {
	if (Weapon == NULL) return;

	Weapon->Reload();
}

void ASimpleShooterCharacter::MoveForward(float Value) {
	if (Value != 0.0f) {
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ASimpleShooterCharacter::MoveRight(float Value) {
	if (Value != 0.0f) {
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ASimpleShooterCharacter::TurnAtRate(float Rate) {
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASimpleShooterCharacter::LookUpAtRate(float Rate) {
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

//////////////////////////////////////////////////////////////////////////
// Networking

void ASimpleShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(ASimpleShooterCharacter, Health);
	DOREPLIFETIME(ASimpleShooterCharacter, Weapon);
}

bool ASimpleShooterCharacter::FireWeapon_Validate() {
	return true; // TODO check fire rate, etc?
}

void ASimpleShooterCharacter::FireWeapon_Implementation() {
	// Make sure we have a weapon equipped
	if (Weapon == NULL) return;

	// Can this weapon be fired?
	if (!Weapon->CanFire()) return;

	Weapon->FireWeapon();
}

//////////////////////////////////////////////////////////////////////////
// Misc

float ASimpleShooterCharacter::GetHealthProportion() {
	return Health / ((float) 100); // TODO max health?
}

void ASimpleShooterCharacter::DealDamage(int amount) {
	Health -= amount;
}

void ASimpleShooterCharacter::SetWeapon(AWeapon *NewWeapon) {
	// If we are currently holding a weapon, detach it
	if (Weapon != NULL)
		Weapon->DetachRootComponentFromParent();

	// Set the new weapon
	Weapon = NewWeapon;

	// Attach the new weapon to the Skeleton
	if (Weapon != NULL)
		Weapon->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true),
								  TEXT("GripPoint"));
}

float ASimpleShooterCharacter::GetAmmoProportion() {
	// If we don't have a gun, say that we're empty.
	if (Weapon == NULL) return 0;

	// Find the ammo and max ammo
	int ammo = Weapon->Ammo;
	int max = Weapon->GetMaxAmmo();

	// Convert it to a fraction.
	// Cast to float first, otherwise the / operator gets performed on two ints.
	return ammo / ((float) max);
}

